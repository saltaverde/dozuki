<?php

	/*
	EXAMPLE USAGE/OUTPUT:
	php > require 'Guide.php';
	php > Step::relateTo('Image', 'images', 'step_id', 'step_id');
	php > Guide::relateTo('Step', 'steps', 'guide_id', 'guide_id');
	php > $guides = Guide::findAll(array(1,2,3));
	php > echo count($guides);
	3
	php > echo json_encode($guides[0], JSON_PRETTY_PRINT);
	{
    	"guide_id": "1",
    	"title": "A very first guide",
    	"description": "Let's begin!",
    	"author": "bcoleman",
    	"created": "2017-05-24"
	}
	php > echo json_encode($guides[0]->steps[0], JSON_PRETTY_PRINT);
	{
    	"step_id": "1",
    	"guide_id": "1",
    	"num": "1",
    	"instruction": "put the thingamajig in the doohickey"
	}
	php > echo json_encode($guides[0]->steps[0]->images[0], JSON_PRETTY_PRINT);
	{
    	"image_id": "1",
    	"guide_id": "1",
    	"step_id": "1",
    	"num": "1",
    	"url": "\/usr\/local\/images\/thingamajig_img_01.png"
	}
	*/

	require_once '/usr/share/php/Doctrine/DBAL/autoload.php';

	use Doctrine\DBAL;

	// Globals. Better place to put this stuff???
	$config = new \Doctrine\DBAL\Configuration();

	$connectionParams = array(
	        'dbname' => 'dozuki',
	        'user' => 'root',
	        'password' => 'Zekeyboy*7255',
	        'host' => 'localhost',
	        'driver' => 'pdo_mysql',
	);

	$find_format = 'select * from %s where %s = ?;';
	$find_all_format = 'select * from %s where %s in (?);';
	$get_format = 'select * from %s where %s = %s;';

	class DozukiObj {
		protected static $_connection; // shared connection among child classes

		// Establish the relationship between this class and another class
		public static function relateTo($class_name, $coll_name, $column_a, $column_b)
		{
			// $column_a references the field in collection $coll_name
			// $column_b references the field in $this->$collectionName
 			static::$relationships[$coll_name] = array($class_name, $column_a, $column_b);
		}

		// Returns an instance whose primary key matches $id
		public static function find($id)
		{
			global $find_format;
			$conn = static::_getConnection();
			$query_str = sprintf($find_format, static::$collectionName, static::$primaryKey);
			$assc = $conn->fetchAssoc($query_str, array($id));
			
			return new static::$className($assc);
		}

		// Returns an array of instances who have primary keys in $id_arr
		public static function findAll($id_arr)
		{
			global $find_all_format;
			$conn = static::_getConnection();
			$query_str = sprintf($find_all_format, static::$collectionName, static::$primaryKey);
			$all = $conn->fetchAll($query_str, array($id_arr), array(\Doctrine\DBAL\Connection::PARAM_INT_ARRAY));

			foreach ($all as &$value) { $value = new static::$className($value); }

			return $all;
		}

		// Overload constructor to dynamically create properties as each inheriting class
		// will likely have different property names
		function __construct($assc = NULL)
		{
			if (!is_null($assc))
			{
				foreach ($assc as $key => $value) { $this->{$key} = $value; }
			}
		}

		// Overload __get() to enable lazy loading of 'child' properties
		function __get($name)
		{
			global $get_format;
			if (!property_exists($this, $name) && array_key_exists($name, static::$relationships)) 
			{
				$conn = static::_getConnection();
				$relationship = static::$relationships[$name];
				$query_str = sprintf($get_format, $name, $relationship[1], $this->{$relationship[2]});
				$relates = $conn->fetchAll($query_str);
				
				// Replace the elements of $relates with a new instance of the class
				$class_name = $relationship[0];
				foreach ($relates as &$value) { $value = new $class_name($value); }
				
				$this->{$name} = $relates;
			}
			return $this->$name;
		}

		// Static connection to db shared amongst children. Trying to avoid overtaxing db
		// Not sure if this is actually an issue, more research required
		protected static function _getConnection()
		{
			if (static::$_connection == null) 
			{
				global $connectionParams, $config;
				static::$_connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
			}

			return static::$_connection;
		}
	}

	// The child classes for this example

	class Guide extends DozukiObj {
		static $relationships = array(), $className = 'Guide', $collectionName = 'guides', $primaryKey = 'guide_id';
	}

	class Step extends DozukiObj {
		static $relationships = array(), $className = 'Step', $collectionName = 'steps', $primaryKey = 'step_id';		
	}

	class Image extends DozukiObj {
		static $relationships = array(), $className = 'Image', $collectionName = 'images', $primaryKey = 'image_id';
	}
